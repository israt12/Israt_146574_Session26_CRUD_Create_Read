<head>
  <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
  <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
  <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%;  z-index: -999999; top: 0px;">

  <style>
        table{
          text-align: center;
          margin :50px auto;
          background-color:rgba(183, 179, 179, 0.15);
        }
    th{
      text-align: center;
      height: 32px;
      color: #5e90c3;
      font-family: serif;
      font-size: 19px;
    }
    td,tr{
       padding: 20px;
      text-align: center;
     }
  </style>
</head><?php
require_once("../../../vendor/autoload.php");

use App\Hobbies\Hobbies;
use App\Message\Message;

$objHobbies=new Hobbies;

$allData=$objHobbies->index("obj");
$serial=1;
echo "<table>";
echo "<th> Serial No</th><th > ID</th><th>Name</th><th>Hobbies</th><th>Action </th>";
foreach($allData as $oneData)

{
  echo "<tr style='height: 40px'>";
  echo "<td> $serial</td>";
  echo "<td> $oneData->id</td>";
  echo "<td> $oneData->name</td>";
  echo "<td> $oneData->hobby</td>";
  echo"
  <td>
        <a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a>
        <a href='edit.php?id=$oneData->id'><button class='btn btn-success'>Edit</button></a>
        <a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a>

   </td>
  ";

  echo "</tr>";
  $serial++;
}
echo "</table>";