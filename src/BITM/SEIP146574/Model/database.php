<?php
namespace App\Model;

use PDO;
use PDOException;


class Database{
    public $DBH;
    public $username="root";
    public $password="";

    public function __construct()
    {
        try
    {
$this->DBH=new PDO("mysql:host=localhost;dbname=atomic_project_b35",$this->username,$this->password);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}
// $objDatabase=new Database();